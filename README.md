# GitLab FAQ and examples

This page is summarizing guidelines how to use **Gitlab** and **git** for source code development or other content tracking.

GitLab is a web service for **git**, the _stupid content tracker_ (according to its [man page](https://manpages.debian.org/stretch/git-man/git.1.en.html)). Git implements distributed repositories and does not make an assumption about a master repository. It is however good to have such a master repository and dedicated strategy how to contribute to it. This is, where GitLab enters the scene. The web service is hosting remote repositories and implements procedures for synchronization and content merging.

In GitLab, projects and associated repositories are created in a _namespace_, this can be a group or a personal namespace.

This description is maintained in a repository and we will use it as an example.

Further general information how to work with GitLab can be found under the following external links:
- [Git - distributed is the new centralized](https://git-scm.com/)
- [Tutorial: It's all connected in GitLab](https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/)

## Terminology
When working with git/GitLab, you will meet a few terms:
- **commit**:
- **branch**: a linear sequence of commits, branches share a common history and develop independently from one point in the history
- **merge**: two branches are brought together and the result commited with a new commit
- **rebase**: the history of a branch is applied to another branch resulting into a new history containing the commits of both branches
- **fork**: repository copy in a user space of GitLab service
- **clone**: local working copy on a machine

Especially the difference between **fork** and **clone** is very fundamental. The fork is always a remote repository, and the local clone can be connected to the repository in the fork and also to the master repository. In addition, the clone can be connected to forks of other developers. 

## Branches

## Roles
There are two very different use cases, the
1. **User role**: A user or guest wants to download the code, compile it and use it.
2. **Developer role**: A developer contributes to the development.
